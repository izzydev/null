﻿using System;
using NUnit.Framework;
using static NUnit.Framework.Assert;

namespace IzzyDev.Null.Tests
{
    public class ThrowIf_Tests
    {
        private readonly ThrowIf_Tests_Fixture _fixture;

        public ThrowIf_Tests()
        {
            _fixture = new ThrowIf_Tests_Fixture();
        }

        [Test]
        public void Throw_on_null_object()
        {
            var sut = _fixture.SetNullAsObject()
                              .ObjectUnderTest;

            Throws<NullReferenceException>(() => sut.ThrowIfNull());
        }

        [Test]
        public void Throw_with_message_on_null_object()
        {
            var sut = _fixture.SetNullAsObject()
                              .ObjectUnderTest;

            var exMsg = _fixture.ExeceptionMessage;
            var ex = Throws<NullReferenceException>(() => sut.ThrowIfNull(exMsg));
            AreEqual(exMsg, ex.Message);
        }

        [Test]
        public void Throw_on_null_Task_Result()
        {
            var sut = _fixture.SetTaskWithNullResultAsObject()
                              .TaskUnderTest;
            
            ThrowsAsync<NullReferenceException>(async () => await sut.ThrowIfResultNull());
        }

        [Test]
        public void Throw_with_message_on_null_Task_Result()
        {
            var sut = _fixture.SetTaskWithNullResultAsObject()
                              .TaskUnderTest;
            
            var exMsg = _fixture.ExeceptionMessage;
            var ex = ThrowsAsync<NullReferenceException>(async () => await sut.ThrowIfResultNull(exMsg));
            AreEqual(exMsg, ex.Message);
        }
    }
}
