﻿using System.Threading.Tasks;

namespace IzzyDev.Null.Tests
{
    public class ThrowIf_Tests_Fixture
    {
        public object ObjectUnderTest { get; private set; }
        public Task<object> TaskUnderTest => (Task<object>)ObjectUnderTest;
        public string ExeceptionMessage { get; } = "ExeceptionMessage";

        public ThrowIf_Tests_Fixture SetNullAsObject()
        {
            ObjectUnderTest = null;
            return this;
        }

        public ThrowIf_Tests_Fixture SetTaskWithNullResultAsObject()
        {
            ObjectUnderTest = Task.FromResult<object>(null);
            return this;
        }
    }
}