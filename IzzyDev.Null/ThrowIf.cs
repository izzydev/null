﻿using System;
using System.Threading.Tasks;

namespace IzzyDev.Null
{
    public static class ThrowIf
    {
        /// <summary>
        /// Throws exception if given object is null. Otherwise returns given reference.
        /// </summary>
        public static TType ThrowIfNull<TType>(this TType entity, string exceptionMessage = null)
        {
            if (entity is Task)
                throw new InvalidOperationException("It seems like you're using this method on some kind of Task. Use ThrowIfResultNull method instead.");

            if (entity == null) throw new NullReferenceException(exceptionMessage);

            return entity;
        }

        /// <summary>
        /// Attaches callback to task with result null check.
        /// </summary>
        public static async Task<TTaskResult> ThrowIfResultNull<TTaskResult>(this Task<TTaskResult> task, string exceptionMessage = null)
        {
            return await task.ContinueWith(t => t.Result.ThrowIfNull(exceptionMessage));
        }
    }
}
