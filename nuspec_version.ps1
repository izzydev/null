﻿$v = $env:APPVEYOR_BUILD_VERSION

$files = gci -Filter "*.nuspec" -Recurse
foreach ($file in $files)
{
    (Get-Content $file.PSPath) | `
    Foreach-Object { $_ -replace "~version~", "$v" } | `
    Set-Content $file.PSPath
}